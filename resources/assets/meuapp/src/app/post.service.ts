import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpEvent } from '@angular/common/http';
import { Post } from './post';

@Injectable()
export class PostService {

  public posts: Post[] = [];

  constructor(private http: HttpClient) {
    // tslint:disable-next-line:quotemark
    this.http.get("/api/").subscribe(
      (posts: any[]) => {
        // tslint:disable-next-line:prefer-const
        for (let p of posts) {
          this.posts.push(
            new Post(
              p.nome, p.titulo, p.subtitulo,
              p.email, p.mensagem, p.arquivo,
              p.id, p.likes
            )
          );
        }
      }
    );
  }

  salvar(post: Post, file: File) {
    const uploadData = new FormData();
    uploadData.append('nome', post.nome);
    uploadData.append('email', post.email);
    uploadData.append('titulo', post.titulo);
    uploadData.append('subtitulo', post.subtitulo);
    uploadData.append('mensagem', post.mensagem);
    uploadData.append('arquivo', file, file.name);

    // tslint:disable-next-line:quotemark
    this.http.post("/api", uploadData, { reportProgress: true, observe: 'events'})
      .subscribe((event: any) => {
        // tslint:disable-next-line:triple-equals
        if (event.type == HttpEventType.Response) {
          // console.log(event);
          // tslint:disable-next-line:prefer-const
          let p: any = event.body;
          this.posts.push(
            new Post(
              p.nome, p.titulo, p.subtitulo,
              p.email, p.mensagem, p.arquivo,
              p.id, p.likes
            )
          );
        }
        // tslint:disable-next-line:triple-equals
        if (event.type == HttpEventType.UploadProgress) {
          console.log('UploadProgress');
          console.log(event);
        }
      });

  }

  like(id: number) {
    this.http.get('/api/like/' + id)
      .subscribe(
        (event: any) => {
          let p = this.posts.find((p) => p.id == id);
          p.likes = event.likes;
        }
      );
  }

  apagar(id: number) {
    // tslint:disable-next-line:quotemark
    this.http.delete("/api/" + id)
      .subscribe( (event) => {
        // console.log(event);
        // tslint:disable-next-line:prefer-const
        let i = this.posts.findIndex((p) => p.id == id);
        if (i >= 0) {
          this.posts.splice(i, 1);
        }
      });
  }


}
